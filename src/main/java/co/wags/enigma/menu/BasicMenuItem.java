package co.wags.enigma.menu;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 *
 * @author ASUS
 */
public class BasicMenuItem {

    private BasicMenu submenu;
    private final String title;
    private final ActionListener onselect;

    public String getTitle() {
        return title;
    }

    public BasicMenuItem(String title, BasicMenu submenu, ActionListener onselect) {
        this.title = title;
        this.submenu = submenu;
        this.onselect = onselect;
    }

    public void setSubMenu(BasicMenu submenu) {
        this.submenu = submenu;
    }

    public void select() {
        if (onselect != null) {
            onselect.actionPerformed(new ActionEvent(this, 0, "select"));
        }
    }

    public BasicMenu getSubMenu() {
        return submenu;
    }

    @Override
    public String toString() {
        return title;
    }
}
