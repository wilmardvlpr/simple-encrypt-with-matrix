package co.wags.enigma;

import co.wags.enigma.menu.BasicMenu;
import co.wags.enigma.menu.BasicMenuItem;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;

/**
 *
 * @author Wilmar Giraldo
 */
public class Main {

    private static final Scanner sc = new Scanner(System.in);
    private static final String CYPHER = " abcdefghijklmnñopqrstuvwxyzABCDEFGHIJKLMNÑOPQRSTUVWXYZ";

    private static String plainMessage;
    private static double[][] originalMessage;
    private static double[][] encryptedMessage;
    private static double[][] key;

    private static final co.wags.enigma.apache.Ops linearOperation = new co.wags.enigma.apache.Ops();
//    private static final co.wags.enigma.ejml.Ops linearOperation = new co.wags.enigma.ejml.Ops();

    public static void main(String[] args) {

//        test();        
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        Main mainApp = new Main();

        BasicMenu currentMenu = mainApp.createMenuSystem();
        while (currentMenu != null) {
            System.out.println(currentMenu);
            System.out.print("Choose an option: ");
            String inp = "";
            try {
                inp = br.readLine();
                currentMenu = currentMenu.doOption(Integer.parseInt(inp));
            } catch (IOException | NumberFormatException ex) {
                System.out.println("Didn't understand " + inp);
            }
        }
    }

    private BasicMenu createMenuSystem() {

        BasicMenu rootMenu = new BasicMenu("ENIGMA - ENCRYPT/DECRYPT SIMPLE MESSAGES USING MATRIX");
        BasicMenuItem goToRootMenu = new BasicMenuItem("Go back to the root menu", null, null);
        goToRootMenu.setSubMenu(rootMenu);

        BasicMenu subMenuEncrypt = new BasicMenu("Encrypt a message");
        BasicMenu subMenuDecrypt = new BasicMenu("Decrypt a matrix");

        subMenuEncrypt.addItem(new BasicMenuItem("Type the message to encrypt", null, getMessage));
//        subMenuEncrypt.addItem(new BasicMenuItem("Generate a compatible key matrix", null, null));
        subMenuEncrypt.addItem(new BasicMenuItem("Type a compatible key matrix", null, typeKeyMatrix));
        subMenuEncrypt.addItem(new BasicMenuItem("Print encrypted message in matrix form", null, encryptMessage));
        subMenuEncrypt.addItem(goToRootMenu);

        subMenuDecrypt.addItem(new BasicMenuItem("Type the matrix to decrypt", null, typeEncryptedMatrix));
        subMenuDecrypt.addItem(new BasicMenuItem("Type a compatible key matrix", null, typeKeyMatrix));
        subMenuDecrypt.addItem(new BasicMenuItem("Print decrypted message", null, decryptMessage));
        subMenuDecrypt.addItem(goToRootMenu);

        rootMenu.addItem(new BasicMenuItem("Encrypt a message", subMenuEncrypt, null))
                .addItem(new BasicMenuItem("Decrypt a matrix", subMenuDecrypt, null));

        return rootMenu;
    }

    /**
     *
     * Transforma una cadena que representa un mensaje, en una matrix, lo mas
     * cuadrada posible, dando prelación a las filas por encima de las columnas
     *
     * @param originalMessage Cadena que representa el mensaje original
     * @param code Cadena con los caracteres que representa el código a utilizar
     * @return Matrix que representa al mensaje
     */
    private static double[][] getEncodedMessageInMatrixForm(String originalMessage, String code) {

        double root = Math.sqrt(originalMessage.length());
        int filas = (root % 1) != 0 ? (((int) Math.floor(root)) + 1) : (int) Math.floor(root);
        int columnas = ((((int) Math.floor(root)) * filas) < originalMessage.length())
                ? filas
                : (int) Math.floor(root);

        System.out.println("Longitud del mensaje: " + originalMessage.length());
//        System.out.println("Raiz cuadrada de la longitud del mensaje: " + root);
//        System.out.println("Parte decimal: " + (root % 1));
        System.out.println("Basado en el texto ingresado, se propone una matrix");
        System.out.println("con dimension [" + filas + ", " + columnas + "]");
        System.out.println("Desea modificar esta dimensión? [s/n]: ");
        String input = sc.next();
        input = input.toLowerCase();

        if ("s".equalsIgnoreCase(input)) {
            boolean correctValues = false;
            do {
                System.out.println("Ingrese la cantidad de filas: ");
                filas = sc.nextInt();

                System.out.println("Ingrese la cantidad de columnas: ");
                columnas = sc.nextInt();

                correctValues = (filas * columnas >= originalMessage.length());
                System.out.println("Nueva dimension: [" + filas + ", " + columnas + "] - " + correctValues);

                if (!correctValues) {
                    System.out.println("Ha ingresado valores incorrectos, intente de nuevo!");
                } else {
                    System.out.println("Los valores ingresados parecen correctos, desea mantener esta dimensión [s/n]? [" + filas + ", " + columnas + "]");
                    input = sc.next();
                    input = input.toLowerCase();
                    correctValues = "s".equalsIgnoreCase(input);
                }
            } while (!correctValues);
        }

        double[][] encodedMessageMatrix = new double[filas][columnas];

        int i = 0;
        while (i < originalMessage.length()) {
            for (int fila = 0; fila < filas; fila++) {
                for (int columna = 0; columna < columnas; columna++) {
                    char currentChar = (i >= originalMessage.length()) ? ' ' : originalMessage.charAt(i); // También puede ser el caracter '*'
                    int encodedCharValue = code.indexOf(currentChar);
                    encodedMessageMatrix[fila][columna] = encodedCharValue;
                    i++;
                }
            }
        }

        return encodedMessageMatrix;
    }

    public static double[][] encrypt(double[][] originalMessage, String code, double[][] keyMatrix) {
        encryptedMessage = linearOperation.multiply(originalMessage, keyMatrix);
        return encryptedMessage;
    }

    public static double[][] decryptInMatrixForm(double[][] encryptedMatrix, double[][] keyMatrix) {
        double[][] decryptedMatrix = linearOperation.multiply(encryptedMatrix, linearOperation.getInverse(keyMatrix));
//        printMatrix(decryptedMatrix);
        double[][] decryptedMatrixRounded = roundMatrix(decryptedMatrix);
        System.out.println("Inverse: ");
        printMatrix(linearOperation.getInverse(keyMatrix));
        System.out.println("Rounded: ");
        printMatrix(decryptedMatrixRounded);
        
        return decryptedMatrixRounded;
    }

    public static String decryptInPlainText(double[][] encryptedMatrix, double[][] keyMatrix, String code) {
        double[][] decryptedMatrix = decryptInMatrixForm(encryptedMatrix, keyMatrix);
        return printMatrixInSingleLine(decryptedMatrix, code);
    }

    /**
     * Transforma el contenido de una matrix, en caracteres, dadas la matrix y
     * el código de caracteres de traducción, las posiciones en la matrix
     * equivalen a las posiciones en el código de caracteres.
     *
     * Imprime la traducción en la salida estandar, como una sola linea.
     *
     * @param matrix Matrix con códigos a traducir
     * @param code Cadena que corresponde al juego de caracteres para la
     * traduccion
     */
    private static String printMatrixInSingleLine(double[][] matrix, String code) {

        StringBuilder sb = new StringBuilder();

        for (int x = 0; x < matrix.length; x++) {
            for (int y = 0; y < matrix[0].length; y++) {
                sb.append(code.charAt((int) matrix[x][y]));
            }
        }
//        System.out.println(sb.toString());
        return sb.toString();
    }

    /**
     * Transforma el contenido de una matrix, en caracteres, dadas la matrix y
     * el código de caracteres de traducción, las posiciones en la matrix
     * equivalen a las posiciones en el código de caracteres.
     *
     * Imprime la traducción en la salida estandar, como una sola linea.
     *
     * @param matrix Matrix con códigos a traducir
     */
    private static void printMatrix(double[][] matrix) {

        for (int x = 0; x < matrix.length; x++) {
            for (int y = 0; y < matrix[0].length; y++) {
                System.out.print(matrix[x][y] + ",  ");
            }
            System.out.println("");
        }
        System.out.println("");
    }

    ActionListener getMessage = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            System.out.println("Ingrese el mensaje a codificar: ");
            plainMessage = sc.nextLine();
            originalMessage = getEncodedMessageInMatrixForm(plainMessage, CYPHER);

            System.out.println("Se va a codificar el siguiente mensaje:");
            System.out.println("\"" + plainMessage + "\"");
            System.out.println("Encoded message [matrix]: ");
            printMatrix(originalMessage);
        }
    };

    ActionListener typeKeyMatrix = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {

            if ((null != plainMessage && null != originalMessage) || null != encryptedMessage) {
                int keySize = null != originalMessage ? originalMessage[0].length : encryptedMessage[0].length;

                double[][] keyMatrix = new double[keySize][keySize];

                System.out.println("The key matrix has be a " + keySize + " x " + keySize + " dimension.");

                for (int row = 0; row < keySize; row++) {
                    for (int column = 0; column < keySize; column++) {
                        System.out.println("Please type the value of the position [" + (row + 1) + ", " + (column + 1) + "]:");
                        keyMatrix[row][column] = sc.nextInt();
                    }
                }

                key = keyMatrix;

                System.out.println("Key for encrypt/decrypt [matrix]: ");
                printMatrix(key);

                if (linearOperation.isValidKey(key)) {
                    System.out.println("The current key as valid.");
                } else {
                    System.out.println("The current key as not valid, because the determinant is 0");
                }
            }
        }

    };

    ActionListener encryptMessage = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            encryptedMessage = encrypt(originalMessage, CYPHER, key);
            System.out.println("Encrypted message [matrix]: ");
            printMatrix(encryptedMessage);
        }
    };

    ActionListener decryptMessage = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            originalMessage = decryptInMatrixForm(encryptedMessage, key);
            System.out.println("Encrypted matrix: ");
            printMatrix(encryptedMessage);

            System.out.println("Decrypted matrix: ");
            printMatrix(originalMessage);

            System.out.println("Decrypted message [Single line]: ");
            System.out.println(printMatrixInSingleLine(originalMessage, CYPHER));
        }
    };

    ActionListener typeEncryptedMatrix = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            System.out.println("How many rows? ");
            int rows = sc.nextInt();

            System.out.println("How many columns? ");
            int columns = sc.nextInt();

            double[][] encrypted = new double[rows][columns];

            System.out.println("The encrypted matrix has be a " + rows + " x " + columns + " dimension.");
            sc.nextLine();
            for (int row = 0; row < rows; row++) {
                for (int column = 0; column < columns; column++) {
                    System.out.println("Please type the value of the position [" + (row + 1) + ", " + (column + 1) + "]:");
                    encrypted[row][column] = sc.nextInt();
                }
            }

            encryptedMessage = encrypted;
            System.out.println("Encrypted message [matrix]: ");
            printMatrix(encryptedMessage);
        }

    };

    private static double[][] roundMatrix(double[][] matrix) {
        for (int x = 0; x < matrix.length; x++) {
            for (int y = 0; y < matrix[0].length; y++) {
                matrix[x][y] = Math.round(matrix[x][y]);
            }
        }
        return matrix;
    }

    private static void test() {

        double[][] matrixB = {
            {0, 3, 4},
            {1, -4, -5},
            {-1, 3, 4}
        };

        printMatrix(linearOperation.getInverse(matrixB));
    }

//    private static boolean isCompatibleMessageKeyPair(String message, SimpleMatrix key) {
//        SimpleMatrix messageInMatrixForm = getEncodedMessageInMatrixForm(message, CYPHER);
//        return messageInMatrixForm.numCols() == key.numRows();
//    }
//
//    private static boolean isCompatibleMessageKeyPair(SimpleMatrix messageInMatrixForm, SimpleMatrix key) {
//        return messageInMatrixForm.numCols() == key.numRows();
//    }
}
