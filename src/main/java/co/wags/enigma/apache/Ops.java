package co.wags.enigma.apache;

import org.apache.commons.math3.linear.Array2DRowRealMatrix;
import org.apache.commons.math3.linear.LUDecomposition;
import org.apache.commons.math3.linear.MatrixUtils;
import org.apache.commons.math3.linear.RealMatrix;

/**
 *
 * @author Wilmar Alexander Giraldo Sanchez
 */
public class Ops {

    /**
     * Obtiene la inversa de una matriz
     *
     * @param matrix
     * @return double[][] inversa de la matriz pasada por parámetros
     */
    public static double[][] getInverse(double[][] matrix) {
//        RealMatrix rmO = MatrixUtils.createRealMatrix(matrix);
//        RealMatrix rmI = MatrixUtils.inverse(rmO);

        RealMatrix rmO = new Array2DRowRealMatrix(matrix);
        RealMatrix rmI = new LUDecomposition(rmO).getSolver().getInverse();

        return rmI.getData();
    }
    
    /**
     * Obtiene la inversa de una matriz con los valores redondeados
     *
     * @param matrix
     * @return double[][] inversa de la matriz pasada por parámetros
     */
    public static double[][] getRoundedInverse(double[][] matrix) {

        RealMatrix rmO = new Array2DRowRealMatrix(matrix);
        RealMatrix rmI = new LUDecomposition(rmO).getSolver().getInverse();
        
        double[][] roundedInverse = roundMatrix(rmI.getData());

        return roundedInverse;
    }

    /**
     * Valida que la matrix clave sea válida, calculando su determinante y
     * evaluando que la determinante hallada sea diferente de cero.
     *
     * @param matrix Matrix cuadrada que contiene la clave
     * @return true si la determinante de la clave es diferente de cero
     */
    public static boolean isValidKey(double[][] matrix) {
        RealMatrix rm = MatrixUtils.createRealMatrix(matrix);
        double md = new LUDecomposition(rm).getDeterminant();
        return (md != 0);
    }

    /**
     * Devuelve el producto de dos matrices
     *
     * @param matrixA Primera matrix operando
     * @param matrixB Segunda matrix operando
     *
     * @return double[][] producto de dos matrices pasadas por parametros
     */
    public static double[][] multiply(double[][] matrixA, double[][] matrixB) {
        RealMatrix rmA = MatrixUtils.createRealMatrix(matrixA);
        RealMatrix rmB = MatrixUtils.createRealMatrix(matrixB);
        RealMatrix rmC = rmA.multiply(rmB);

        return rmC.getData();
    }
    
    /**
     * Dada una matrix, devuelve una de igual dimensiones con los valores redondeados
     * @param matrix
     * @return 
     */ 
    private static double[][] roundMatrix(double[][] matrix) {
        for (int x = 0; x < matrix.length; x++) {
            for (int y = 0; y < matrix[0].length; y++) {
                matrix[x][y] = Math.round(matrix[x][y]);
            }
        }
        return matrix;
    }

}
