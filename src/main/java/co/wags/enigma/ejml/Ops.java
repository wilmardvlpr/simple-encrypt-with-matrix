package co.wags.enigma.ejml;

import org.ejml.simple.SimpleMatrix;

/**
 *
 * @author Wilmar Alexander Giraldo Sanchez
 */
public class Ops {

    /**
     * Obtiene la inversa de una matriz
     *
     * @param matrix
     * @return double[][] inversa de la matriz pasada por parámetros
     */
    public static double[][] getInverse(double[][] matrix) {
        SimpleMatrix smO = new SimpleMatrix(matrix);
        SimpleMatrix smI = smO.invert();

        return getInitMatrix(smI);
    }
    
    /**
     * Obtiene la inversa de una matriz con sus valores redondeados
     *
     * @param matrix
     * @return double[][] inversa de la matriz pasada por parámetros
     */
    public static double[][] getRoundedInverse(double[][] matrix) {
        SimpleMatrix smO = new SimpleMatrix(matrix);
        SimpleMatrix smI = smO.invert();

        double[][] roundedInverse = roundMatrix(getInitMatrix(smI));

        return roundedInverse;
    }

    /**
     * Valida que la matrix clave sea válida, calculando su determinante y
     * evaluando que la determinante hallada sea diferente de cero.
     *
     * @param matrix Matrix cuadrada que contiene la clave
     * @return true si la determinante de la clave es diferente de cero
     */
    public static boolean isValidKey(double[][] key) {
        SimpleMatrix sm = new SimpleMatrix(key);
        
        return ( sm.determinant() != 0 );
    }

    /**
     * Devuelve el producto de dos matrices
     *
     * @param matrixA Primera matrix operando
     * @param matrixB Segunda matrix operando
     *
     * @return double[][] producto de dos matrices pasadas por parametros
     */
    public static double[][] multiply(double[][] matrixA, double[][] matrixB) {
        SimpleMatrix smA = new SimpleMatrix(matrixA);
        SimpleMatrix smB = new SimpleMatrix(matrixB);
        SimpleMatrix smC = smA.mult(smB);

        return getInitMatrix(smC);
    }

    /**
     * Devuelve los datos de un objeto SimpleMatrix en forma de matriz de
     * doubles
     *
     * @param matrix
     * @return datos en forma de matriz de dobles
     */
    private static double[][] getInitMatrix(SimpleMatrix matrix) {
        double[][] doubles = new double[matrix.numRows()][matrix.numCols()];
        for (int x = 0; x < matrix.numRows(); x++) {
            for (int y = 0; y < matrix.numCols(); y++) {
                doubles[x][y] = matrix.get(x, y);
            }
        }
        return doubles;
    }
    
    /**
     * Dada una matrix, devuelve una de igual dimensiones con los valores redondeados
     * @param matrix
     * @return 
     */ 
    private static double[][] roundMatrix(double[][] matrix) {
        for (int x = 0; x < matrix.length; x++) {
            for (int y = 0; y < matrix[0].length; y++) {
                matrix[x][y] = Math.round(matrix[x][y]);
            }
        }
        return matrix;
    }

}
