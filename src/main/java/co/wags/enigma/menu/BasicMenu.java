package co.wags.enigma.menu;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author ASUS
 */
public class BasicMenu {

    private final List<BasicMenuItem> items = new ArrayList<>();
    private final String title;

    public BasicMenu(String title) {
        this.title = title;
    }

    public BasicMenu doOption(int option) {
        if (option == 0) {
            return null;
        }
        option--;
        if (option >= items.size()) {
            System.out.println("Unknown option " + option);
            return this;
        }
        items.get(option).select();
        BasicMenu next = items.get(option).getSubMenu();

        return next == null ? this : next;
    }

    public BasicMenu addItem(BasicMenuItem item) {
        items.add(item);
        return this;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(title).append("\n");
        for (int i = 0; i < title.length(); i++) {
            sb.append("-");
        }
        sb.append("\n");
        for (int i = 0; i < items.size(); i++) {
            sb.append((i + 1)).append(") ").append(items.get(i)).append("\n");
        }
        sb.append("0) Quit");
        return sb.toString();
    }
}
